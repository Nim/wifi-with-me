Dependencies
============

In order to facilitate dependency management, you can use a pip and
a virtual environment (like virtualenv).

Install packages:

     # apt-get install python3-pip python3-virtualenv libyaml-dev

Create and activate the virtualenv with:

     $ virtualenv -p $(which python3) venv
     $ source venv/bin/activate

We use django framework.  Install it from requirements with pip:

     $ pip install -r requirements/base.txt

For development, install `dev.txt` instead:

     $ pip install -r requirements/dev.txt

Set up configuration
====================

Then create and edit a `wifiwithme/settings/local.py` configuration file
`wifiwithme/settings/local.py`.

Mandatory settings
------------------

You **must** Define some details about your ISP in the ISP variable, eg:

    ISP={
        'NAME':'FAIMAISON',
        'TITLE':'Réseau wifi expérimental',
        'SITE':'//www.faimaison.net',
        'EMAIL':'bureau (at) faimaison.net',
        'ZONE':'Nantes et environs',
        'ADDRESS_EXAMPLE': 'rue du calvaire, nantes',
        'URL_CONTACT':'//www.faimaison.net/pages/contact.html',
        'LATITUDE':47.218371,
        'LONGITUDE':-1.553621,
        'ZOOM':13,
        'CNIL':{
            'LINK':'//www.faimaison.net/files/cnil-faimaison-1819684-withwithme.pdf',
            'NUMBER':1819684
        }
    }

    SITE_URL='https://wifi.faimaison.net

Optional settings
-----------------

### Website URL

The wifi-with-me website URL (for links included in emails :)

    SITE_URL="http://example.tld"


## URL Prefix

Optionaly, you can define an url prefix (ex: `/foo/`) so that wifi-with-me is
accessible under *http://example.com/foo/* :

    URL_PREFIX='foo/'

### Email Notifications

If you want to receive email notifications on each new contrib, customize those :

List of notification recipients:

    NOTIFICATION_EMAILS=['admin@example.tld']

Notification sender address:

    DEFAULT_FROM_EMAIL='notifier@example.tld'

### Webhook Notifications

If you want to send web notifications on each new contrib, install
[requests](http://docs.python-requests.org/en/master/) and customize those :

    WEBHOOK = True
    WEBHOOK_URL = 'https://<domain>/<path>'
    WEBHOOK_KEY = '<secret>'

Then, on each contrib, an HTTP POST request wil be made to `WEBHOOK_URL` with a json dict containing `'key': WEBHOOK_KEY`
and the notification as `'text'`.

### Data expiration

The data gets deleted after one year, if the contributing user does not give
its explicit consent to keep it one more year.

Reminders are sent to the contribution author when expiration date gets
close. By default we send two notifications :

    DATA_EXPIRATION_REMINDERS = [
        30,  # 1 month before
        7,   # 1 week before
    ]

You can tweak it to your will or decide to send no reminder (with value `[]`).


Migrate from bottle version (optional)
======================================

If you used the (old) bottle version of wifi-with-me and want to migrate your
data follow this extra step :

    $ ./manage.py migrate auth
    $ ./manage.py migrate contribmap 0001 --fake

Run development server
======================

It is required to initialize database first:

    $ ./manage.py migrate

Create an admin:

    $ ./manage.py createsuperuser

Then launch service with:

    $ ./manage.py runserver

You can visit with your browser at <http://127.0.0.1:8000/map/contribute>

Drop the database
=================

If you want to **reset all your data**.

    $ rm db.sqlite3


Run production server
=====================

(Pretty rough instructions. Feel free to submit patches)

1. Make sure to set `DJANGO_SETTINGS_MODULE` environment variable to
   `wifiwithme.settings.prod`, eg:

        $ export DJANGO_SETTINGS_MODULE=wifiwithme.settings.prod

2. Deploy it [like any django site](https://docs.djangoproject.com/en/1.11/howto/deployment/)
3. Customize [mandatory and optional settings](#set-up-configuration)
4. Customize `SECRET_KEY` to something really random. Hint: `python -c "import string,random; uni=string.ascii_letters+string.digits+string.punctuation; print(repr(''.join([random.SystemRandom().choice(uni) for i in range(random.randint(45,50))])))"
5. Set *daily* crons for the two commands that take care of data expiration
   handling. (do not forget `DJANGO_SETTINGS_MODULE`, and use the `--help` of
   these commands)
    - `./manage.py delete_expired_contribs`
    - `./manage.py send_expiration_reminders`
