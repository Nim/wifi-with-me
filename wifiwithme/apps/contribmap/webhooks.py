import json

from django.conf import settings


def send_moderator_webhook(contrib, request=None):
    """ Sends a notification to a webhook
    """
    if settings.WEBHOOK:
        import requests
        text = "Nouvelle demande de %s" % contrib.name
        if request is not None:
            text += ": %s" % contrib.get_absolute_url(request)
        requests.post(
            settings.WEBHOOK_URL, data=json.dumps({
                'text': text,
                'key': settings.WEBHOOK_KEY,
            }))
