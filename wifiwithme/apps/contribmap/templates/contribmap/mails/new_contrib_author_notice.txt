Chèr·e {{ contrib.name }},

Votre demande a bien été enregistrée. Elle est en ligne publiquement à l'adresse : <{{ permalink }}>.

Si tout ou partie des informations n'apparaissent pas, c'est que vous avez choisi qu'elles ne soient pas publiques.

Vous pouvez gérer ou supprimer votre demande grâce à ce lien privé à conserver :

<{{ management_link }}>

Bien à vous,

Les bénévoles de {{ isp.NAME }}
