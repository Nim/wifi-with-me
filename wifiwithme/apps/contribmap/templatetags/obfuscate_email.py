from django import template
register = template.Library()

@register.filter
def obfuscate_email ( email ):
    return email.replace('@', ' [at] ')
