# You must set a customized an non versioned SECRET_KEY in production mode

DEBUG = False
SECRET_KEY = None

from .base import *

try:
    from .local import *
except ImportError:
    pass

STATIC_URL = '/{}assets/'.format(URL_PREFIX)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s',
            'datefmt': "%d/%b/%Y %H:%M:%S",
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
     },
     'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
        },
    },
}
